package src.com.route;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class route_planner_main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		   List<Route> routeInfo=new ArrayList<Route>();
		   List<Route> array=new ArrayList<Route>();
		   String fromCity;
		   routeinfodisplay d1= new routeinfodisplay();
		   System.out.println("------------Task-1---------------");
	       routeInfo=d1.displayDetails();
	       Scanner sc=new Scanner(System.in);
	       System.out.println("Enter source city: ");
	       fromCity=sc.nextLine();
	       System.out.println("\n");
	       System.out.println("------------Task-2---------------");
		   array=d1.showDirectFlights(routeInfo,fromCity);
		   System.out.println("\n");
	       System.out.println("------------Task-3---------------");
		   d1.sortDirectFlights(array);
	       System.out.println("Enter source city: ");
	       fromCity=sc.nextLine();
	       System.out.println("Enter to city: ");
	       String toCity=sc.nextLine();
		   System.out.println("\n");
		   System.out.println("Displaying the all connections");
		   System.out.println("------------Task-4---------------");
		   d1.showAllConnections(routeInfo, "Delhi", "London",0,"Delhi", "London");
	}

}
