package src.com.route;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.lang.String;

class EmployeeComparator implements Comparator<Route>{

	
	public int compare(Route e1, Route e2) {
		if(e1.getTo().compareTo(e2.getTo())<0)
			return -1;
		else if((e1.getTo().compareTo(e2.getTo())>0))
			return 1;
		else
			return 0;
	}
}
public class routeinfodisplay {

	ArrayList<Route> routeInfo=new ArrayList<Route>();
	public List<Route> displayDetails() {
		String fileName="src\\src\\com\\route\\routes.csv";
		FileReader fr=null;
		BufferedReader br=null;
		try
		{
			fr=new FileReader(fileName);
			br=new BufferedReader(fr);
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		try
		{
			String line=br.readLine();
			while((line=br.readLine())!=null)
			{
				String[] details=line.split("	");
				Route r=new Route(details[0],details[1],Integer.parseInt(details[2]),details[3],details[4]);
				routeInfo.add(r);
				
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		System.out.println("Displaying flight route details.");
		for(Route i:routeInfo)
		{
			System.out.println(i);
		}
		return routeInfo;
	}
	public List<Route> showDirectFlights(List<Route> routeInfo,String fromCity)
	{
		int c=0;
		List<Route> array=new ArrayList<Route>();
		System.out.println("Displaying source to all destination cities");
		for(Route i:routeInfo)
		{
			if(i.getFrom().equals(fromCity))
			{
				System.out.println(i);
				array.add(i);
				c=c+1;
			}
		}
		if(c==0)
		{
			System.out.println("We are sorry. At this point of time, we do not have any information on flights originating from Amsterdam.");
			
		}
		return array;
	}
	
	public void sortDirectFlights(List<Route> array)
	{
		Collections.sort(array,new EmployeeComparator());
		System.out.println("Displaying source to all cities in order");
		for(Route i:array)
		{
				System.out.println(i);
		}
	}
	public void showAllConnections(List<Route> routeInfo, String fromCity,String toCity,int directcount,String city,String tocity)
	{
		for(Route i:routeInfo)
		{
				if(fromCity.equals(i.getFrom()))
				{
					if(toCity.equals(i.getTo()))
					{
						if(directcount==0)
						{
						System.out.println(i);
						directcount+=1;
						}
						else
						{
							for(Route j:routeInfo)
							{
								
								if(city.equals(j.getFrom())&& j.getTo().equals(i.getFrom()))
								{
									System.out.println(j);
								}
							}
							System.out.println(i);
						}
					}
					else
					{
						showAllConnections(routeInfo,i.getTo(),toCity,directcount,city,tocity);
					}
				}
		}
	}
}
