package src.com.route;
public class Route{
	private String from;
	private String to;
	private int distance;
	private String travel_time;
	private String airfare;
	
	Route(String from,String to,int distance,String travel_time,String airfare)
	{
		this.from=from;
		this.to=to;
		this.distance=distance;
		this.travel_time=travel_time;
		this.airfare=airfare;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getTravel_time() {
		return travel_time;
	}

	public void setTravel_time(String travel_time) {
		this.travel_time = travel_time;
	}

	public String getAirfare() {
		return airfare;
	}

	public void setAirfare(String airfare) {
		this.airfare = airfare;
	}
	@Override
	public String toString()
	{
		return from+" "+to+" "+distance+" "+travel_time+" "+airfare;
	}
}

